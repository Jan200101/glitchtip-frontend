export const intRegex = new RegExp(/^[0-9]+$/);

export const urlRegex = new RegExp(
  /^[A-Za-z][A-Za-z\d.+-]*:\/*(?:\w+(?::\w+)?@)?[^\s/]+(?::\d+)?(?:\/[\w#!:.?+=&%@\-/]*)?$/
);
